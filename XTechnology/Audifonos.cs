﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XTechnology
{
    class Audifonos
    {
        private Audifonos alambricos ;

        public Audifonos Alambrico

        {
            get { return alambricos ; }
            set { alambricos  = value; }
        }
        private Audifonos inalambricos;

        public Audifonos Inalambricos
        {
            get { return inalambricos; }
            set { inalambricos = value; }
        }

    }
}
