﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XTechnology
{
    class Clientes
    {
        public Clientes(string pCedula,string pNombres,string pApellidos,char pContrasenia, string pCorreo)
        {
            Cedula = pCedula;
            Nombres = pNombres;
            Apellidos = pApellidos;
            Contrasenia = pContrasenia;
            Correo = pCorreo;
        }
        private string Cedula;

        public string cedula
        {
            get { return Cedula; }
            set { Cedula = value; }
        }
        private string  Nombres;

        public string  nombres
        {
            get { return Nombres; }
            set { Nombres = value; }
        }
        private string Apellidos;

        public string apellidos
        {
            get { return Apellidos; }
            set { Apellidos = value; }
        }
        private char Contrasenia ;

        public char contrasenia             
        {
            get { return Contrasenia; }
            set { Contrasenia = value; }
        }
        private string Correo;

        public string correo
        {
            get { return Correo; }
            set { Correo = value; }
        }
        public void Dispositivos()
        {
            List<Smartphone> obj = new List<Smartphone>();
            
            List<Laptop> obj1 = new List<Laptop>();

            List<Audifonos> obj2 = new List<Audifonos>();

            List<Television> obj3 = new List<Television>();
        }
    }
}
